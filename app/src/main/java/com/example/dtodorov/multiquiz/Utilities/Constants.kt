package com.example.dtodorov.multiquiz.Utilities

const val BASE_URL = "https://dstodorov-portfolio-api.herokuapp.com/"
const val URL_REGISTER = "${BASE_URL}user/signup"
const val URL_LOGIN = "${BASE_URL}user/login"
const val URL_GET_USER = "${BASE_URL}user/"
const val URL_GET_QUESTIONS = "${BASE_URL}question"
const val URL_GET_ANSWERS = "${BASE_URL}answer"
const val URL_UPDATE_USER = "${BASE_URL}user/"