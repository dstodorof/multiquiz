package com.example.dtodorov.multiquiz.Controllers

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.example.dtodorov.multiquiz.R
import com.example.dtodorov.multiquiz.Services.AuthService
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        loginSpinner.visibility = View.INVISIBLE

    }

    fun signUpButtonClicked(view: View) {
        val signUpIntent = Intent(this, RegisterUser::class.java)
        startActivity(signUpIntent)
    }

    fun loginButtonClicked(view: View) {
        enableSpinner(true)
        val email = loginEmailTxt.text.toString()
        val password = loginPasswordTxt.text.toString()
        if (email.isNotEmpty() && password.isNotEmpty()) {
            AuthService.loginUser(this, email, password) { loginSuccess ->
                if (loginSuccess) {
                    Log.d("Block:", "loginSuccess")
                    AuthService.findUserByEmail(this) { findSuccess ->
                        Log.d("Block:", "findSuccess")
                        if (findSuccess) {
                            enableSpinner(false)
                            loginEmailTxt.text.clear()
                            loginPasswordTxt.text.clear()
                            val mainMenuIntent = Intent(this, MainMenu::class.java)
                            startActivity(mainMenuIntent)
                        } else {
                            Toast.makeText(this, "Can't find the user details", Toast.LENGTH_LONG).show()
                            enableSpinner(false)
                        }
                    }
                } else {
                    Toast.makeText(this, "Something went wrong :(", Toast.LENGTH_LONG).show()
                    enableSpinner(false)
                }
            }
        } else {
            Toast.makeText(this, "Please fill in both email and password", Toast.LENGTH_LONG).show()
            enableSpinner(false)
        }
    }

    fun enableSpinner(enable: Boolean) {
        if (enable) {
            loginSpinner.visibility = View.VISIBLE
        } else {
            loginSpinner.visibility = View.INVISIBLE
        }
        loginButton.isEnabled = !enable
        signupButton.isEnabled = !enable
    }
}
