package com.example.dtodorov.multiquiz.Controllers

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.dtodorov.multiquiz.R
import com.example.dtodorov.multiquiz.Services.AuthService
import com.example.dtodorov.multiquiz.Services.QuestionService
import com.example.dtodorov.multiquiz.Services.UserDataService
import kotlinx.android.synthetic.main.activity_main_menu.*

class MainMenu : AppCompatActivity() {
    private var userExperienceTextBar = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_menu)
        progressBar.visibility = View.INVISIBLE
        menuUserExperienceBar.max = UserDataService.currentMax.toInt()
        userExperienceTextBar = UserDataService.experience + " / " + menuUserExperienceBar.max

        menuUsername.text = UserDataService.username
        menuUserLevel.text = UserDataService.level
        menuUserExperience.text = userExperienceTextBar
        menuUserExperienceBar.progress = UserDataService.experience.toInt()
    }

    fun menuNewGameBtnClicked(view: View) {
        enableSpinner(true)
        QuestionService.loadQuestions(this) { loadQuestionsSuccess ->
            if (loadQuestionsSuccess) {
                enableSpinner(false)
                val newGameIntent = Intent(this, NewGame::class.java)
                startActivity(newGameIntent)
            }
        }
    }

    fun menuLogoutBtnClicked(view: View) {
        UserDataService.logout()
        finish()
    }

    private fun enableSpinner(enable: Boolean) {
        if (enable) {
            progressBar.visibility = View.VISIBLE
        } else {
            progressBar.visibility = View.INVISIBLE
        }
        menuNewGameBtn.isEnabled = !enable
        menuSettingsBtn.isEnabled = !enable
        menuUserProfileBtn.isEnabled = !enable
        menuLogoutBtn.isEnabled = !enable
    }

    override fun onResume() {
        super.onResume()
        enableSpinner(true)
        AuthService.findUserByEmail(this) {success ->
            if(success) {
                enableSpinner(false)
                menuUserExperienceBar.max = UserDataService.currentMax.toInt()
                userExperienceTextBar = UserDataService.experience + " / " + menuUserExperienceBar.max
                menuUserLevel.text = UserDataService.level
                menuUserExperience.text = userExperienceTextBar
                menuUserExperienceBar.progress = UserDataService.experience.toInt()
            }
        }
    }
}
