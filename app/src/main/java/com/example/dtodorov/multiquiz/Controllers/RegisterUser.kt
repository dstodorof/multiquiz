package com.example.dtodorov.multiquiz.Controllers

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.example.dtodorov.multiquiz.R
import com.example.dtodorov.multiquiz.Services.AuthService
import kotlinx.android.synthetic.main.activity_register_user.*

class RegisterUser : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_user)
        createSpinner.visibility = View.INVISIBLE
    }

    fun registerUserButtonClicked(view: View) {
        enableSpinner(true)
        val username = registerUsernameTxt.text.toString()
        val email = registerEmailTxt.text.toString()
        val password = registerPasswordTxt.text.toString()
        if (username.isNotEmpty() && email.isNotEmpty() && password.isNotEmpty()) {
            AuthService.registerUser(this, username, email, password) { registerSuccess ->
                if(registerSuccess) {
                    Toast.makeText(this, "User has been created. You can login now!", Toast.LENGTH_LONG).show()
                    enableSpinner(false)
                    finish()
                } else {
                    errorToast()
                }
            }
        } else{
            Toast.makeText(this, "Make sure user name, email and password are not empty", Toast.LENGTH_LONG).show()
            enableSpinner(false)
        }
    }

    private fun enableSpinner(enable: Boolean) {
        if(enable){
            createSpinner.visibility = View.VISIBLE
        } else {
            createSpinner.visibility = View.INVISIBLE
        }

        registerUserButton.isEnabled = !enable
    }

    fun errorToast(){
        Toast.makeText(this, "Something went wrong, please try again", Toast.LENGTH_LONG).show()
        enableSpinner(false)
    }
}
