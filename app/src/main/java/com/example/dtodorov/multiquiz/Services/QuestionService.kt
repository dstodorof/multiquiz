package com.example.dtodorov.multiquiz.Services

import android.content.Context
import android.util.Log
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.example.dtodorov.multiquiz.Models.Question
import com.example.dtodorov.multiquiz.Utilities.URL_GET_QUESTIONS
import com.android.volley.toolbox.Volley
import com.example.dtodorov.multiquiz.Utilities.URL_GET_ANSWERS
import org.json.JSONException


object QuestionService {

    var questions = ArrayList<Question>()
    fun loadQuestions(context: Context, complete: (Boolean) -> Unit) {
        val getQuestionsRequest = object : JsonObjectRequest(Method.GET, URL_GET_QUESTIONS, null, Response.Listener { questionsResponse ->
            clearQuestions()
            val questionsJsonResponse = questionsResponse.getJSONArray("questions")
            try {
                for (x in 0 until questionsJsonResponse.length()) {
                    val questionJson = questionsJsonResponse.getJSONObject(x)
                    val id = questionJson.getString("id")
                    val question = questionJson.getString("question")
                    val category = questionJson.getString("category")
                    val difficultyLevel = questionJson.getString("difficultyLevel")
                    val experiencePoints = questionJson.getString("experiencePoints")
                    val answer1 = ""
                    val answer2 = ""
                    val answer3 = ""
                    val answer4 = ""
                    val correctAnswer = ""

                    val newQuestion = Question(id, question, category, difficultyLevel, experiencePoints, answer1, answer2, answer3, answer4, correctAnswer)
                    questions.add(newQuestion)
                }

                assignAnswers(context) { assignSuccessful ->
                    if (assignSuccessful) {
                        println("Answers was successfully loaded")
                        complete(true)
                    } else {
                        println("Answers was not successfully loaded")
                        complete(false)
                    }
                }
            } catch (e: JSONException) {
                complete(false)
                Log.d("JSON Error:", e.localizedMessage)
            }
        }, Response.ErrorListener { error ->
            complete(false)
            Log.d("QUESTIONS ERROR", error.localizedMessage)
        }) {
            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }

            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["Authorization"] = "Bearer ${AuthService.userToken}"
                return headers
            }
        }
        Volley.newRequestQueue(context).add(getQuestionsRequest)
    }

    fun assignAnswers(context: Context, complete: (Boolean) -> Unit) {
        val getAnswersRequest = object : JsonObjectRequest(Method.GET, URL_GET_ANSWERS, null, Response.Listener { answersResponse ->
            val answersJsonResponse = answersResponse.getJSONArray("answers")
            try {
                for (y in 0 until answersJsonResponse.length()) {
                    val answerJson = answersJsonResponse.getJSONObject(y)
                    for (i in 0 until questions.size) {
                        val answerQuestionId = answerJson.getString("questionId")
                        if (questions[i].id == answerQuestionId) {
                            when {
                                questions[i].answer1.isEmpty() -> questions[i].answer1 = answerJson.getString("answer")
                                questions[i].answer2.isEmpty() -> questions[i].answer2 = answerJson.getString("answer")
                                questions[i].answer3.isEmpty() -> questions[i].answer3 = answerJson.getString("answer")
                                questions[i].answer4.isEmpty() -> questions[i].answer4 = answerJson.getString("answer")
                            }
                            if (answerJson.getBoolean("isCorrect")) {
                                questions[i].correctAnswer = answerJson.getString("answer")
                            }
                        }
                    }
                }
                complete(true)
            } catch (e: JSONException) {
                complete(false)
                Log.d("JSON Error:", e.localizedMessage)
            }
        }, Response.ErrorListener { error ->
            Log.d("ANSWERS", error.localizedMessage)
            complete(false)
        }) {
            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }

            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["Authorization"] = "Bearer ${AuthService.userToken}"
                return headers
            }
        }
        Volley.newRequestQueue(context).add(getAnswersRequest)
    }

    fun clearQuestions() {
        questions.clear()
    }
}