package com.example.dtodorov.multiquiz.Controllers

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.dtodorov.multiquiz.R
import com.example.dtodorov.multiquiz.Services.UserDataService
import kotlinx.android.synthetic.main.activity_game_results.*
import kotlin.math.roundToInt

class GameResults : AppCompatActivity() {
    var correctAnswers = ""
    var wrongAnswers = ""
    var collectedXP = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game_results)

        correctAnswers = "Correct answers: " + intent.getStringExtra("correctAnswers")
        correctAnswersTxt.text = correctAnswers
        wrongAnswers = "Wrong answers: " + intent.getStringExtra("wrongAnswers")
        wrongAnswersTxt.text = wrongAnswers
        collectedXP = "Collected XP: " + intent.getStringExtra("collectedXP")
        collectedXPTxt.text = collectedXP
    }

    fun gameResultsFinishBtnClicked(view: View) {
        val props = HashMap<String, String>()

        var playerLevel = UserDataService.level.toInt()
        val playerMaxExperience = UserDataService.getMaxExperience(playerLevel)
        val collectedExperience = collectedXP.substringAfter(": ").toInt()
        var currentExperience = UserDataService.experience.toInt()
        val totalExperience = currentExperience + collectedExperience

        if (totalExperience >= playerMaxExperience) {
            currentExperience = totalExperience - playerMaxExperience
            if (totalExperience == playerMaxExperience) {
                currentExperience++
            }
            playerLevel++
            UserDataService.currentMax = playerMaxExperience.toString()
            props["experience"] = currentExperience.toString()
            props["level"] = playerLevel.toString()
        } else {
            props["experience"] = totalExperience.toString()
        }

        UserDataService.updateUserInfo(this, UserDataService.id, props) { success ->
            if (success) {
                println("User has been updated!")
            } else {
                println("Problem on user updating!")
            }
        }
        finishGame()
    }

    private fun finishGame() {
        finish()
    }
}
