package com.example.dtodorov.multiquiz.Controllers

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.dtodorov.multiquiz.R
import com.example.dtodorov.multiquiz.Services.QuestionService
import kotlinx.android.synthetic.main.activity_new_game.*
import java.util.*
import kotlin.collections.ArrayList

class NewGame : AppCompatActivity() {
    private var questionObjectNumber = 0
    private var questionNumber = 1

    private var wrongAnswers = 0
    private var correctAnswers = 0
    private var collectedXP = 0


    private val listOfNumbers = arrayListOf<Int>()
    private lateinit var questionsIndexes: ArrayList<Int>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_game)
        questionsIndexes = prepareQuestionsIndexes()
        setQuestion(questionsIndexes[questionObjectNumber])
        questionObjectNumber++
        questionNumber++
    }

    fun newGameAnswer1Clicked(view: View) {
        pushAnswer(newGameAnswer1.text.toString())
    }

    fun newGameAnswer2Clicked(view: View) {
        pushAnswer(newGameAnswer2.text.toString())
    }

    fun newGameAnswer3Clicked(view: View) {
        pushAnswer(newGameAnswer3.text.toString())
    }

    fun newGameAnswer4Clicked(view: View) {
        pushAnswer(newGameAnswer4.text.toString())
    }

    private fun pushAnswer(answer: String) {
        val givenAnswer = answer.substringAfter(".").trim()
        val correctAnswer = QuestionService.questions[questionsIndexes[questionObjectNumber - 1]].correctAnswer //-1 Because we increase it on the first question
        checkAnswer(givenAnswer, correctAnswer)
        if (questionNumber > 10) { //When the game is finished
//            println("Game is finished!")
//            println("-----------------STATS-----------------")
//            println("Correct Answers: $correctAnswers")
//            println("Wrong Answers: $wrongAnswers")
//            println("Collected XP: $experienceDuringGame")
//            println("---------------------------------------")
            val gameResultIntent = Intent(this, GameResults::class.java)
            gameResultIntent.putExtra("correctAnswers", correctAnswers.toString())
            gameResultIntent.putExtra("wrongAnswers", wrongAnswers.toString())
            gameResultIntent.putExtra("collectedXP", collectedXP.toString())
            startActivity(gameResultIntent)
            finish()

        } else {
            setQuestion(questionsIndexes[questionObjectNumber])
            questionObjectNumber++
            questionNumber++
        }
    }

    private fun setQuestion(questionObjectNumber: Int) {
        val questionTemplate = "$questionNumber. ${QuestionService.questions[questionObjectNumber].question}"
        val answer1Template = "A. " + QuestionService.questions[questionObjectNumber].answer1
        val answer2Template = "B. " + QuestionService.questions[questionObjectNumber].answer2
        val answer3Template = "C. " + QuestionService.questions[questionObjectNumber].answer3
        val answer4Template = "D. " + QuestionService.questions[questionObjectNumber].answer4
        newGameQuestion.text = questionTemplate
        newGameAnswer1.text = answer1Template
        newGameAnswer2.text = answer2Template
        newGameAnswer3.text = answer3Template
        newGameAnswer4.text = answer4Template
    }

    private fun prepareQuestionsIndexes(): ArrayList<Int> {
        val rawArray = java.util.ArrayList<Int>()
        val randomizedArray = java.util.ArrayList<Int>()
        for (i in 0 until QuestionService.questions.size) {
            rawArray.add(i)
        }
        for (i in 0..9) {
            val rand = Random()
            val randomIndex = rand.nextInt(rawArray.size)
            randomizedArray.add(rawArray[randomIndex])
            rawArray.removeAt(randomIndex)
        }
        return randomizedArray
    }

    private fun checkAnswer(givenAnswer: String, correctAnswer: String) {
        if (givenAnswer == correctAnswer) {
            //When the answer is correct
            correctAnswers++
            collectedXP += QuestionService.questions[questionObjectNumber].experiencePoints.toInt()
        } else {
            //When the answer is not correct
            wrongAnswers++
        }
    }
}
