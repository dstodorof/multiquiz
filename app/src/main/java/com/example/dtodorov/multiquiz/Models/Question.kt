package com.example.dtodorov.multiquiz.Models

class Question (val id: String,
                val question: String,
                val category: String,
                val difficultyLevel: String,
                val experiencePoints: String,
                var answer1: String,
                var answer2: String,
                var answer3: String,
                var answer4: String,
                var correctAnswer: String)