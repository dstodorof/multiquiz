package com.example.dtodorov.multiquiz.Services

import android.content.Context
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.dtodorov.multiquiz.Utilities.URL_UPDATE_USER
import org.json.JSONArray
import org.json.JSONObject
import kotlin.math.roundToInt

object UserDataService {
    var id = ""
    var username = ""
    var email = ""
    var level = ""
    var rank = ""
    var experience = ""
    var currentMax = ""

    fun logout() {
        id = ""
        username = ""
        email = ""
        level = ""
        rank = ""
        experience = ""
        AuthService.isLoggedIn = false
    }

    fun updateUserInfo(context: Context, userId: String, properties: HashMap<String, String>, complete: (Boolean) -> Unit) {
        val jsonBody = JSONArray()
        val urlTemplate = "$URL_UPDATE_USER$userId"
        for (property in properties) {
            val jsonObject = JSONObject()
            jsonObject.put("propName", property.key)
            jsonObject.put("value", property.value)
            jsonBody.put(jsonObject)
        }

        val requestBody = jsonBody.toString()
        val updateUser = object : StringRequest(Method.PATCH, urlTemplate, Response.Listener { response ->
            complete(true)
        }, Response.ErrorListener { error ->
            complete(false)
        }) {
            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }

            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers.put("Authorization", "Bearer ${AuthService.userToken}")
                return headers
            }

            override fun getBody(): ByteArray {
                return requestBody.toByteArray()
            }
        }
        Volley.newRequestQueue(context).add(updateUser)
    }

    fun convertExperienceToLevel(currentExperience: Int): HashMap<String, Int> {
        val experienceDetails = HashMap<String, Int>()
        var level = 1
        var experience = currentExperience

        var currentMax = 100
        if (experience < 100) {
            experienceDetails["level"] = level
            experienceDetails["experience"] = experience
            experienceDetails["currentMax"] = currentMax
        } else {
            for (i in 1 until experience) {
                if (i > currentMax) {
                    experience -= currentMax
                    currentMax = (currentMax * 1.2).roundToInt()
                    level++

                    if (currentMax >= experience) {
                        experienceDetails["level"] = level
                        experienceDetails["experience"] = experience
                        experienceDetails["currentMax"] = currentMax
                        break
                    }
                }
            }
        }
        return experienceDetails
    }

    fun getMaxExperience(playerLevel: Int): Int {
        var maxExperience = 100

        if (level.toInt() > 1) {
            for (i in 1 until level.toInt()) {
                maxExperience = (maxExperience * 1.2).roundToInt()
            }
        }
        return maxExperience
    }
}